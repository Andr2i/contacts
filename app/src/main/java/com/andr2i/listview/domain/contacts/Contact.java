package com.andr2i.listview.domain.contacts;

import java.io.Serializable;

public class Contact implements Serializable {
    public static final int SAMPLE_MAX_CONTACT_COUNT = 20;
    private final int id;
    private final String name;
    private final String address;

    public Contact(int id, String name,String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()){
            return false;
        }
        Contact message = (Contact) o;
        return id == message.id;
    }

    @Override
    public int hashCode() {
        return id;
    }
}
