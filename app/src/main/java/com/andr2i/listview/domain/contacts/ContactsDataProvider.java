package com.andr2i.listview.domain.contacts;

import java.util.List;

import io.reactivex.Observable;

public interface ContactsDataProvider {
    Observable<List<Contact>> getAllContacts();
    Integer addContact();
}
