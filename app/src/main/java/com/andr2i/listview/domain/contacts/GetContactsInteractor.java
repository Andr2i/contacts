package com.andr2i.listview.domain.contacts;

import com.andr2i.listview.domain.base.Interactor;

import java.util.List;



import io.reactivex.Observable;
import io.reactivex.Scheduler;


public class GetContactsInteractor extends Interactor<List<Contact>, Void> {

    private final ContactsDataProvider contactsDataProvider;


    public GetContactsInteractor(Scheduler jobScheduler,
                                 Scheduler iuScheduler,
                                 ContactsDataProvider contactsDataProvider) {
        super(jobScheduler, iuScheduler);
        this.contactsDataProvider = contactsDataProvider;
    }

    @Override
    protected Observable<List<Contact>> buildObservable(Void parameter) {
        return contactsDataProvider.getAllContacts();
    }
}
