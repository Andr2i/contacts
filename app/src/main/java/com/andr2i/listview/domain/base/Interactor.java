package com.andr2i.listview.domain.base;




import androidx.core.util.Preconditions;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;


public abstract class Interactor<ResultType, ParameterType> {

    private final CompositeDisposable subscription = new CompositeDisposable ();

    protected final Scheduler jobScheduler;
    private final Scheduler uiScheduler;

    public Interactor(Scheduler jobScheduler, Scheduler uiScheduler) {
        this.jobScheduler = jobScheduler;
        this.uiScheduler = uiScheduler;
    }

    protected abstract Observable<ResultType> buildObservable(ParameterType parameter);

    public void execute(ParameterType parameter, DisposableObserver<ResultType> subscriber) {

        final Observable<ResultType> observable = this.buildObservable(parameter)
                .subscribeOn(jobScheduler)
                .observeOn(uiScheduler);
        addDisposable(observable.subscribeWith(subscriber));
    }

    protected void addDisposable(Disposable disposable) {
        Preconditions.checkNotNull(disposable);
        Preconditions.checkNotNull(subscription);
        subscription.add(disposable);
    }


    public void execute(DisposableObserver<ResultType> subscriber) {

        execute(null, subscriber);
    }

    public void unsubscribe() {
        subscription.clear();
    }
}