package com.andr2i.listview.data;

import com.andr2i.listview.domain.contacts.Contact;
import com.andr2i.listview.domain.contacts.ContactsDataProvider;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;


public class ContactsDataProviderImpl implements ContactsDataProvider {

    List<Contact> contacts;

    @Override
    public Observable<List<Contact>> getAllContacts() {
        if (contacts == null) {
            contacts = new ArrayList<>();
            for (int i = 0; i < Contact.SAMPLE_MAX_CONTACT_COUNT; i++) {
                contacts.add(new Contact(i, "contact" + i, "address" + i));
            }
        }
        return Observable.just(contacts);
    }

    @Override
    public Integer addContact() {
        Integer i = contacts.size() + 1;
        contacts.add(new Contact(i, "contact" + i, "address" + i));
        return i;
    }

}
