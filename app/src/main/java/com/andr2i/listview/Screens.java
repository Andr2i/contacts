package com.andr2i.listview;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

import com.andr2i.listview.presentation.contacts.ContactsFragment;
import com.andr2i.listview.presentation.main.MainActivity;

public class Screens {


    public static final class  ContactsScreen extends Screen {

        @Override
        public Fragment getFragment() {
            return ContactsFragment.newInstance();
        }
    }


    public static final class MainScreen extends Screen {
        @Override

        public Intent getActivityIntent(Context context) {
            return new Intent(context, MainActivity.class);
        }
    }

}
