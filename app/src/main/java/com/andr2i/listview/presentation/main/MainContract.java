package com.andr2i.listview.presentation.main;


import com.andr2i.listview.presentation.common.BaseActivityContract;
import com.andr2i.listview.presentation.common.BasePresenter;

public interface MainContract {
    interface View extends BaseActivityContract {


    }

    interface Presenter extends BasePresenter<View> {
        void init();
    }
}
