package com.andr2i.listview.presentation.common;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;



public abstract class BaseFragment<Binding extends ViewDataBinding> extends Fragment {
    private Binding binding;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        attachFragment();
    }

    @Override
    public void onStop() {
        stopFragment();
        super.onStop();
    }

    @Override
    public void onStart() {
        super.onStart();
        startFragment();
    }

    @Override
    public void onPause() {
        pauseFragment();
        super.onPause();
    }

    protected abstract BasePresenter getPresenter();


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, getLayoutRes(), container, false);
        initFragmentView();
        return binding.getRoot();
    }

    protected Binding getBinding() {
        return binding;
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract void initFragmentView();

    protected abstract void attachFragment();

    protected abstract void startFragment();

    protected abstract void stopFragment();

   // protected abstract void destroyFragment();

    protected abstract void pauseFragment();

    @Override
    public void onDetach() {
       // destroyFragment();
        if (getPresenter() != null){
            getPresenter().detachView();
        }
        super.onDetach();
    }

    @Override
    public void onDestroy() {

        super.onDestroy();
    }



}