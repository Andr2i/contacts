package com.andr2i.listview.presentation.contacts;

import android.view.View;

import com.andr2i.listview.data.ContactsDataProviderImpl;
import com.andr2i.listview.domain.contacts.Contact;
import com.andr2i.listview.domain.contacts.ContactsDataProvider;
import com.andr2i.listview.domain.contacts.GetContactsInteractor;
import java.util.List;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;


public class ContactsPresenter implements IContactContract.Presenter{

    private final GetContactsInteractor getContactsInteractor;

    private IContactContract.IView view;
    private ContactsDataProvider data;

    public ContactsPresenter(){

         data = new ContactsDataProviderImpl();
         this.getContactsInteractor = new GetContactsInteractor(Schedulers.io(), AndroidSchedulers.mainThread(),data);
    }


    @Override
    public void startView(IContactContract.IView view) {
        this.view = view;
          getContactsInteractor.execute(new DisposableObserver<List<Contact>>() {
            @Override
            public void onError(Throwable e) {
                // getView().showError(R.string.error);
            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onNext(List<Contact> contacts) {
                getView().setContacts(contacts);
            }
        });
    }

    public IContactContract.IView getView() {
        return view;
    }

    @Override
    public void detachView() {
        getContactsInteractor.unsubscribe();
    }

    @Override
    public void addContact() {
        getView().notifyInserted(data.addContact());
    }
}