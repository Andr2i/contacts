package com.andr2i.listview.presentation.contacts;


import android.view.View;

import com.andr2i.listview.domain.contacts.Contact;
import com.andr2i.listview.presentation.common.BasePresenter;
import java.util.List;

public interface IContactContract {

    interface IView  {
        void setContacts(List<Contact> contacts);
        void notifyInserted(Integer position);
    }

    interface Presenter extends BasePresenter<IView> {
        void addContact();
    }


}
