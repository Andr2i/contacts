package com.andr2i.listview.presentation.contacts;

import android.os.Bundle;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.andr2i.listview.R;
import com.andr2i.listview.databinding.RecyclerViewBinding;
import com.andr2i.listview.domain.contacts.Contact;
import com.andr2i.listview.presentation.common.BaseFragment;
import com.andr2i.listview.presentation.common.BasePresenter;

import java.util.List;


public class ContactsFragment  extends BaseFragment<RecyclerViewBinding> implements IContactContract.IView {

    private IContactContract.Presenter presenter;
    private static final String TAG_DATA = "data";
    private RecyclerView recyclerView;
    private  ContactsAdapter contactsAdapter;
    public ContactsFragment() {

    }


    public static ContactsFragment newInstance() {
        ContactsFragment fragment = new ContactsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.recycler_view;
    }

    @Override
    protected void initFragmentView() {
        presenter = new ContactsPresenter();
       getBinding().setHandlerFab(presenter);
    }

    @Override
    protected void attachFragment() {

    }

    @Override
    protected void startFragment() {
        presenter.startView(this);
        Integer id = R.id.contact_recycler_view;
        recyclerView =  (RecyclerView)getView().findViewById(id);
    }

    @Override
    protected void stopFragment() {

    }


    @Override
    protected void pauseFragment() {

    }

    @Override
    public void setContacts(List<Contact> contacts) {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        contactsAdapter = new ContactsAdapter(contacts,presenter);
        recyclerView.setAdapter(contactsAdapter);

    }

    @Override
    public void notifyInserted(Integer position) {
        contactsAdapter.notifyItemInserted(position);
    }

}
