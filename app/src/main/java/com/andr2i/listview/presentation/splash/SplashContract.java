package com.andr2i.listview.presentation.splash;

import com.andr2i.listview.presentation.common.BasePresenter;

public interface SplashContract {
    interface View {
        void message(String s);
//interface View extends BaseActivityContract {
    }

    interface Presenter extends BasePresenter<View> {
        void pause();
    }
}
