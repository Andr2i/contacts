package com.andr2i.listview.presentation;


import com.andr2i.listview.Screen;
import com.andr2i.listview.presentation.common.BaseActivity;

public interface RouterContract {

    void onStart(BaseActivity view, int containerId);

    void navigateTo(Screen screen);

    //void transactionCalcFragment();

   // void transactionMainActivity();

//    void backStack();
//
//    void isBack(boolean val);
//
//    void header(String val);
//
//    void message(String val);
//
//    void isProgress(boolean val);

}
