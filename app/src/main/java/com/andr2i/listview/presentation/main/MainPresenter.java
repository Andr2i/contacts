package com.andr2i.listview.presentation.main;


import com.andr2i.listview.Screens;
//import com.andr2i.listview.model.Calculator;
//import com.andr2i.listview.model.ICalculator;
import com.andr2i.listview.presentation.Router;

//
public class MainPresenter implements MainContract.Presenter {

    private MainContract.View view;
   // private ICalculator model;


    public MainPresenter() {
      //  this.model = new Calculator();

    }


    @Override
    public void startView(MainContract.View view) {
       this.view = view;
    }

    @Override
    public void detachView() {
        if (view != null)
            view = null;
    }


    @Override
    public void init() {
       // Router.getInstanse().transactionCalcFragment();
        Router.getInstanse().navigateTo(new Screens.ContactsScreen());
    }


}
