package com.andr2i.listview.presentation;

import android.content.Intent;


import com.andr2i.listview.Screen;
import com.andr2i.listview.presentation.common.BaseActivity;

public class Router implements RouterContract {
   // private BaseActivityContract view;
    private BaseActivity view;
    private static RouterContract instanse;
    private int containerId;

    private Router() {

    }

    public static synchronized RouterContract getInstanse() {
        if (instanse == null) {
            instanse = new Router();
        }
        return instanse;
    }

  //  @Override
    public void onStart(BaseActivity view, int containerId) {

        this.view = view;
        this.containerId = containerId;
    }



    @Override
    public void navigateTo(Screen screen) {
        Intent activityIntent = screen.getActivityIntent(view);
        // Start activity
        if (activityIntent != null) {
            view.checkAndStartActivity(activityIntent);
        } else {
            view.fragmentReplace(screen.getFragment(), containerId);
        }
    }



//
//    @Override
//    public void backStack() {
//        view.backStack();
//    }
//
//    @Override
//    public void isBack(boolean val) {
//        view.isBack(val);
//    }
//
//    @Override
//    public void header(String val) {
//        view.header(val);
//    }
//
//    @Override
//    public void message(String val) {
//        view.message(val);
//    }
//
//    @Override
//    public void isProgress(boolean val) {
//        view.isProgress(val);
//    }
}
