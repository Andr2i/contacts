package com.andr2i.listview.presentation.contacts;

import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.andr2i.listview.R;
import com.andr2i.listview.databinding.ContactItemBinding;
import com.andr2i.listview.domain.contacts.Contact;

import java.util.List;

public class ContactsAdapter extends RecyclerView.Adapter<ContactsAdapter.ContactViewHolder>{

    private final List<Contact> contacts;
    private View.OnClickListener onItemClickListener;
    private IContactContract.Presenter presenter;


    public ContactsAdapter(List<Contact> contacts,IContactContract.Presenter presenter) {
        this.presenter = presenter;
        this.contacts = contacts;
    }

    public void setOnItemClickListener(View.OnClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_item, parent, false);
        return new ContactViewHolder(convertView,presenter);
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {
        holder.bind(contacts.get(position));
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }



    //------------------------Holder---------------------------------//

    public class ContactViewHolder extends RecyclerView.ViewHolder  {

        private IContactContract.Presenter presenter;
        ContactItemBinding binding;

        public ContactViewHolder(View itemView,IContactContract.Presenter presenter ) {
            super(itemView);

            this.presenter = presenter;
            itemView.setOnClickListener(onItemClickListener);

            binding = DataBindingUtil.bind(itemView);
            if (binding != null && presenter != null)
                binding.setHandler(this);
        }

        public void bind(Contact contact) {
            binding.contactName.setText(contact.getName());
            binding.contactAddress.setText(contact.getAddress());
            itemView.setTag(contact);

        }


        private void deleteItemFromList(View v, final int position) {

            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());

            builder.setTitle("Dlete ");
            builder.setMessage("Delete Item ?")
                    .setCancelable(false)
                    .setPositiveButton("CONFIRM",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    contacts.remove(position);
                                    notifyDataSetChanged();

                                }
                            })
                    .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {


                        }
                    });

            builder.show();

        }

         public void onDelete(View view) {
             deleteItemFromList(view, this.getAdapterPosition());
        }
    }
}
