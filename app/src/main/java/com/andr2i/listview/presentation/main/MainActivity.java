package com.andr2i.listview.presentation.main;


import android.content.Context;
import android.view.View;

import com.andr2i.listview.R;


import com.andr2i.listview.databinding.ActivitySpalshBinding;
import com.andr2i.listview.presentation.Router;
import com.andr2i.listview.presentation.common.BaseActivity;
import com.andr2i.listview.presentation.common.BasePresenter;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


import androidx.fragment.app.Fragment;

public class MainActivity extends BaseActivity<ActivitySpalshBinding> implements MainContract.View {

    private MainContract.Presenter presenter;

    public MainActivity()
    {

    }
    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView() {
        Router.getInstanse().onStart(this,R.id.content);
        presenter = new MainPresenter();
        presenter.init();
    }

    @Override
    protected void onStartView() {
        presenter.startView(this);
    }

//    @Override
//    protected void onDestroyView() {
//        presenter.detachView();
//    }

    @Override
    protected BasePresenter getPresenter() {
        return presenter;
    }





    @Override
    public void transactionFragmentNoBackStack(Fragment fragment, int container) {
          super.transactionFragmentNoBackStack(fragment,container);
    }

    @Override
    public void transactionFragmentWithBackStack(Fragment fragment, int container) {
        super.transactionFragmentWithBackStack(fragment,container);
    }

    @Override
    public void transactionActivity(Class<?> activity, boolean cycleFinish) {
        super.transactionActivity(activity,cycleFinish);
    }

    @Override
    public <T> void transactionActivity(Class<?> activity, boolean cycleFinish, T... object) {
        super.transactionActivity(activity,cycleFinish,object);
    }

    @Override
    public void message(String val) {
        toast(val);
    }

    @Override
    public void isProgress(boolean val) {

    }

    @Override
    public void backStack() {
        super.onBackPressed();
    }

    @Override
    public void isBack(boolean val) {

    }

    @Override
    public void header(String val) {

    }

    @Override
    public Context getContext() {
        return this;
    }

}
