package com.andr2i.listview.presentation.common;

public interface BasePresenter<V> {

    void startView(V view);

    void detachView();
}
