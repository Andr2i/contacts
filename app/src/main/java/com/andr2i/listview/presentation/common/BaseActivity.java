package com.andr2i.listview.presentation.common;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;


public abstract class BaseActivity<Binding extends ViewDataBinding> extends AppCompatActivity {
    private Binding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, getLayoutRes());

        initView();
    }


    public void checkAndStartActivity(Intent activityIntent) {
        startActivity(activityIntent);
            this.finish();
    }

    public void fragmentReplace(Fragment fragment,int containerId)
    {
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(containerId,fragment,fragment.getClass().getSimpleName())
                .commit();
    }




    protected void transactionFragmentNoBackStack(Fragment fragment, int container){
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(container,fragment,fragment.getClass().getSimpleName())
                .commit();
    }


    protected void transactionFragmentWithBackStack(Fragment fragment, int container) {
        this.getSupportFragmentManager()
                .beginTransaction()
                .replace(container, fragment, fragment.getClass().getSimpleName())
                .addToBackStack(null)
                .commit();
    }

    protected void transactionActivity(Class<?> activity,boolean cycleFinish){
        if (activity != null) {
            Intent intent = new Intent(this, activity);
            startActivity(intent);
            if(cycleFinish) {
                this.finish();
            }
        }
    }

    protected <T>void transactionActivity(Class<?> activity,boolean cycleFinish,T... object){
        if (activity != null) {
            Intent intent = new Intent(this, activity);
            if(object != null){

            }
            startActivity(intent);
            if(cycleFinish) {
                this.finish();
            }
        }
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        FragmentManager manager = this.getSupportFragmentManager();
        if (manager.getBackStackEntryCount() == 0) {
            super.finish();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        onStartView();
    }

    @LayoutRes
    protected abstract int getLayoutRes();

    protected Binding getBinding(){
        return binding;
    }

    protected abstract void initView();


    @Override
    public void onDestroy() {
        if (getPresenter() != null) {
           // onDestroyView();
            getPresenter().detachView();
        }
        super.onDestroy();
    }

    protected abstract void onStartView();

   // protected abstract void onDestroyView();

    protected abstract BasePresenter getPresenter();



    protected void toast(String message){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show();
    }




}
