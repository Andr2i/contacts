package com.andr2i.listview;

import android.content.Context;
import android.content.Intent;

import androidx.fragment.app.Fragment;

public abstract class Screen {

    public Fragment getFragment() {
        return null;
    }

    public Intent getActivityIntent(Context context) {
        return null;
    }
}